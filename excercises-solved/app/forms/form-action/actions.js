'use server'

export async function signInFormAction(formData) {
    await db.saveUser();

    // Mimic network error
    // throw new Error("Failed to sign in, please try again later.");

    return {
        name: formData.get("name"),
        email: formData.get("email")
    };
}

const db = {
    saveUser: async () => {
        await new Promise(resolve => setTimeout(resolve, 2000));
    }
}