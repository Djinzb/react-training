import styles from "../page.module.scss";
import Link from "next/link";

export default function FormsPage() {
    return (
        <main className={`${styles.main} ${styles.form}`}>
            <h1>Forms exercises</h1>
            <ul className={styles.links}>
                <li><Link href={"/forms/form-action"}>Form server action</Link></li>
            </ul>
        </main>
    );
}
