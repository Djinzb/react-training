# NextJs training exercises

## Slides
- [Link](https://docs.google.com/presentation/d/1o-6qnih9DRVjeaRQYayxTlcSys15D44fYa50NjLoaa8/edit?usp=sharing)

## Goal
- Learn how to use NextJs
- Learn how to manage state with different solutions
  - React local component state
  - React context or global state
  - External state management library (Jotai, Recoil, etc)
- Learn form actions with React Server Components
- 
## Useful links
- [NextJs docs](https://nextjs.org/docs)
- [Nextjs server/client example](https://app-router.vercel.app/context)
- [React (new docs)](https://react.dev/reference/react)
- [React hooks](https://react.dev/reference/rules/rules-of-hooks)
- [React context](https://react.dev/reference/react/hooks#context-hooks)
