import {atom} from "jotai";

export const userInfoJotai = atom({name: "Not signed in", email: ""});