"use client"
import {createContext, useContext, useState} from 'react';

export const UserContext = createContext(null);

export function useUser() {
    return useContext(UserContext);
}

export function UserProvider({ children }) {
    const [user, setUser] = useState({name: "Not signed in", email: ""});
    return <UserContext.Provider value={[user, setUser]}>{children}</UserContext.Provider>;
}


