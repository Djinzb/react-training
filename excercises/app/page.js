import styles from "./page.module.scss";
import Link from "next/link";

export default function Home() {
    return (
        <main className={styles.main}>
            <h1>Nextjs/React training exercises</h1>
            <ul className={styles.links}>
                <li><Link href={"/forms"}>Forms</Link></li>
            </ul>
        </main>
    );
}
