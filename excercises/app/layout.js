import {Inter} from "next/font/google";
import "./globals.scss";
import {UserProvider} from "@/state/context/UserContext";

const inter = Inter({subsets: ["latin"]});

export const metadata = {
    title: "NextJS Training exercises",
    description: "By AAIT COMMV",
};

export default function RootLayout({children}) {
    return (
        <html lang="en">
        <body className={inter.className}>
        <UserProvider>
            {children}
        </UserProvider>
        </body>
        </html>
    );
}
