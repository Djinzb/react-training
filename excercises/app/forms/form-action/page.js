"use client"
import styles from "../../page.module.scss";
import {NavBar} from "@/components/general/navbar";
import {signInFormAction} from "@/app/forms/form-action/actions";
import {useAtom} from "jotai";
import {userInfoJotai} from "@/state/external-library/global";
import {useState} from "react";

export default function FormActionPage() {
    const [pending, setPending] = useState(false)
    const [error, setError] = useState(null)
    // 1) Local state solution
    // const [userLocalState, setUser] = useState({name: "Not signed in", email: ""});
    // const user = userLocalState;

    // 2) Global state solution
    // const [userGlobalState, setUser] = useUser();
    // const user = userGlobalState;

    // 3) External library solution
    const [userExternalLibrary, setUser] = useAtom(userInfoJotai);
    const user = userExternalLibrary;

    const signInUser = async (formData) => {
        setPending(true)
        setError(null) // Clear previous errors when a new request starts
        let userData;
        try {
            userData = await signInFormAction(formData);
        } catch (error) {
            setError(error.message)
            console.error(error)
        } finally {
            setPending(false);
            setUser(userData);
        }
    };

    return (
        <main className={styles.formPage}>
            <NavBar user={user}/>
            <section className={styles.content}>
                <h1>Sign in:</h1>
                <form action={signInUser}>
                    <label htmlFor="name">Name</label>
                    <input type="text" placeholder="Name" name="name"/>
                    <label htmlFor="email">Email</label>
                    <input type="email" placeholder="Email" name={"email"}/>
                    <button type="submit">{pending ? "Sending..." : "Submit"}</button>
                    {error && (<p className={styles.errorMessage}>{error}</p>)}
                </form>
            </section>
        </main>
    );
}
