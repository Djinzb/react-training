'use server'

export async function signInFormAction(formData) {
    await db.saveUser();

    // Mimic network error
    // throw new Error("Failed to sign in, please try again later.");

    // use form data to create user object
}

const db = {
    saveUser: async () => {
        await new Promise(resolve => setTimeout(resolve, 2000));
    }
}