import styles from "@/app/page.module.scss";

export function NavBar({user}) {
    return (
        <nav>
            <ul>
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/forms">All forms examples</a>
                </li>
            </ul>
            <div className={styles.userInfo}>
                <p>{user?.name || "Not signed in"}</p>
                <p className={styles.userIcon}>{getInitials(user?.name || "Not signed in")}</p>
            </div>
        </nav>
    );
}

function getInitials(name) {
    return name === "Not signed in" ? "?" : name.split(' ').map(word => word.charAt(0).toUpperCase()).join('');
}